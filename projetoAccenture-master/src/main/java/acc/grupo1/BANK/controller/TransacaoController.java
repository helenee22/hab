package main.java.acc.grupo1.BANK.controller;

import java.util.Date;
import java.util.List;

import main.java.acc.grupo1.BANK.repository.Transacao;

public class TransacaoController {
	
	private TransacaoRepository repository;
	
	public List<Transacao> findAll() {
		return repository.findAll();
	}
	
	public List<Transacao> findByIdClienteOrigem(Integer idClienteOrigem) {
		return repository.findByIdClienteOrigem(idClienteOrigem);
	}
	
	public List<Transacao> findByNumeroContaOrigem(int numeroContaOrigem) {
		return repository.findByNumeroContaOrigem(numeroContaOrigem);
	}
	
	public List<Transacao> findByNumeroContaDestino(int numeroContaDestino) {
		return repository.findByNumeroContaDestino(numeroContaDestino);
	}
	
	public List<Transacao> findByData(Date data) {
		return repository.findByData(data);
	}
	
	public Transacao findById(Integer id) {
		return repository.findById(id);
	}
	
	public Transacao save(Transacao transacao) {
		return repository.save(transacao);
	}
}
