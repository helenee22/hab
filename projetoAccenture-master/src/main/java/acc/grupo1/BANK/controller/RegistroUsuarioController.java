package acc.grupo1.BANK.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import acc.grupo1.BANK.entity.Cliente;
import acc.grupo1.BANK.service.UserService;
import acc.grupo1.BANK.web.dto.UserRegistrationDto;



@Controller
@RequestMapping("/")
public class RegistroUsuarioController{

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("cliente") Cliente cliente, @Valid UserRegistrationDto userDto, BindingResult result){

    	Cliente existing = userService.findByCpf(userDto.getCpf());
        if (existing != null){
            result.rejectValue("cpf", null, "CPF ja cadastrado");
        }

        if (result.hasErrors()){
            return "/login";
        }

        userService.save(userDto);
        return "redirect:/login/?success";
    }

}