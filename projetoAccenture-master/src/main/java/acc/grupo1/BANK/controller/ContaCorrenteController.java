package main.java.acc.grupo1.BANK.controller;

import java.util.List;

public class ContaCorrenteController {
	
	private ContaCorrenteRepository repository;
	
	public List<ContaCorrente> findAll() {
		return repository.findAll();
	}
	
	public List<ContaCorrente> findByNumeroDaConta(int numeroDaConta) {
		return repository.findByNumeroDaConta(numeroDaConta);
	}
	
	public List<ContaCorrente> findByNomeClienteAndCpfCliente(String nomeCliente, String cpfCliente) {
		return repository.findByNomeClienteAndCpfCliente(nomeCliente, cpfCliente);
	}
	
	public ContaCorrente findByCpfCliente(String cpf) {
		return repository.findByCpfCliente(cpf);
	}
	
	public ContaCorrente findByIdCliente(Integer idCliente) {
		return repository.findByIdCliente(idCliente);
	}
}
