package main.java.acc.grupo1.BANK.controller;

import java.util.List;

import main.java.acc.grupo1.BANK.repository.Poupanca;

public class PoupancaController {
	
	private PoupancaRepository repository;

	public List<Poupanca> findAll() {
		return repository.findAll();
	}
	
	public List<Poupanca> findByIdCliente(String idCliente) {
		return repository.findByIdCliente(idCliente);
	}
	
	public Poupanca findByNumeroConta(int numeroConta) {
		return repository.findByNumeroConta(numeroConta);
	}
	
	public Poupanca findById(Integer id) {
		return repository.findById(id);
	}
	
	public Poupanca save(Poupanca poupanca) {
		return repository.save(poupanca);
	}
}
