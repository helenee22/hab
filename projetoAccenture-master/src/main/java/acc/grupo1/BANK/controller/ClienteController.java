package acc.grupo1.BANK.controller;

import acc.grupo1.BANK.entity.Cliente;
import acc.grupo1.BANK.repository.ClienteRepository;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

@Controller
public class ClienteController {
	
	private ClienteRepository repository;
	
	@Transactional
	public List<Cliente> findAll() {
		return repository.findAll();
	}
	
	@Transactional
	public List<Cliente> findByNomeCompleto(String nomeCompleto) {
		return repository.findByNomeCompleto(nomeCompleto);
	}
	
	@Transactional
	public List<Cliente> findByAtivo(boolean ativo) {
		return repository.findByAtivo(ativo);
	}
	
	@Transactional
	public List<Cliente> findByEmail(String email) {
		return repository.findByEmail(email);
	}
	
	@Transactional
	public Cliente findByCpf(String cpf) {
		return repository.findByCpf(cpf);
	}
	
	@Transactional
	public Cliente findByIdCliente(Integer idCliente) {
		return repository.findByIdCliente(idCliente);
	}

	@Transactional
	public Cliente save(Cliente cliente) {
		return repository.save(cliente);
	}
}