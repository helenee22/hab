package acc.grupo1.BANK.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import acc.grupo1.BANK.entity.Cliente;


@Controller
public class MainController {

	private ClienteController clienteController;
//	@GetMapping("/index")
//	public String index() {
//		return "index";
//	}
	
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String form(@ModelAttribute("cliente") Cliente cliente) {
		
		clienteController.save(cliente);
		
		return "sucesso";
		
	}

	@GetMapping("/login")
	public String login(Model model) {
		Cliente cliente = new Cliente();
		model.addAttribute("cliente", cliente);
		return "login";
	}

	@GetMapping("/user")
	public String userIndex() {
		return "user/index";
	}
}
