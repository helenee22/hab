package main.java.acc.grupo1.BANK.repository;

import java.util.List;

public interface ContaCorrenteRepository extends JpaRepository<ContaCorrente, Long> {
	
	public List<ContaCorrente> findAll();
	
	public List<ContaCorrente> findByNumeroDaConta(int numeroDaConta);
	
	public List<ContaCorrente> findByNomeClienteAndCpfCliente(String nomeCliente, String cpfCliente);
	
	public ContaCorrente findByCpfCliente(String cpf);
	
	public ContaCorrente findByIdCliente(Integer idCliente);

}
