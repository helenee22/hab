package acc.grupo1.BANK.repository;

import acc.grupo1.BANK.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	public List<Cliente> findAll();
	
	public List<Cliente> findByNomeCompletoAndCpf(String nomeCompleto, String cpf);
	
	public List<Cliente> findByAtivo(boolean ativo);
	
	public List<Cliente> findByEmail(String email);
	
	public Cliente findByCpf(String cpf);
	
	public Cliente findByIdCliente(Integer idCliente);

}