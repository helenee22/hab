package main.java.acc.grupo1.BANK.repository;

import java.util.List;

public interface PoupancaRepository extends JpaRepository<Poupanca, Long> {
	
	public List<Poupanca> findAll();
	
	public List<Poupanca> findByIdCliente(String idCliente);
	
	public Poupanca findByNumeroConta(int numeroConta);
	
	public Poupanca findById(Integer id);
	
	public Poupanca save(Poupanca poupanca);

}
