package main.java.acc.grupo1.BANK.repository;

import java.util.Date;
import java.util.List;

import projetoAccenture-master.src.main.java.acc.grupo1.BANK.entity.Transacao;-master.src.main.java.acc.grupo1.BANK.entity.Transacao;

public interface TransacaoRepository extends JpaRepository<Transacao, Long> {
	
	public List<Transacao> findAll();
	
	public List<Transacao> findByIdClienteOrigem(Integer idClienteOrigem);
	
	public List<Transacao> findByNumeroContaOrigem(int numeroContaOrigem);
	
	public List<Transacao> findByNumeroContaDestino(int numeroContaDestino);
	
	public List<Transacao> findByData(Date data);
	
	public Transacao findById(Integer id);
	
	public Transacao save(Transacao transacao);
	
}
