package acc.grupo1.BANK.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import acc.grupo1.BANK.entity.Cliente;
import acc.grupo1.BANK.web.dto.UserRegistrationDto;



public interface UserService extends UserDetailsService {


	Cliente save(UserRegistrationDto registration);

	Cliente findByCpf(String cpf);
}