package acc.grupo1.BANK.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import acc.grupo1.BANK.controller.ClienteController;
import acc.grupo1.BANK.entity.Cliente;

import acc.grupo1.BANK.web.dto.UserRegistrationDto;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
    private ClienteController clienteController;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public List<Cliente> findByEmail(String email){
        return clienteController.findByEmail(email);
    }

    public Cliente save2(UserRegistrationDto registration){
    	Cliente cliente = new Cliente();
    	cliente.setNome(registration.getNome());
    	cliente.setSobrenome(registration.getSobrenome());
    	cliente.setEmail(registration.getCpf());
    	cliente.setPassword(passwordEncoder.encode(registration.getPassword()));
        return clienteController.save(cliente);
    }

    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    	List<Cliente> user = clienteController.findByEmail(email);
        if (user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
		return null;}


    private Collection<? extends GrantedAuthority> mapRolesToAuthorities() {
		return null;
//        return collection.stream()
//        		.map(role -> new SimpleGrantedAuthority(role.getName()))
//        		.collect(Collectors.toList());
    }


	@Override
	public Cliente save(acc.grupo1.BANK.web.dto.UserRegistrationDto registration) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Cliente findByCpf(String cpf) {
		// TODO Auto-generated method stub
		return null;
	}
}
