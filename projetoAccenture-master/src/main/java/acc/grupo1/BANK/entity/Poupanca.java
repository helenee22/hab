package main.java.acc.grupo1.BANK.entity;

import java.text.DecimalFormat;

@Entity
@Table(name = "POUPANCA")
@Getter
@Setter
@NoArgsConstructor
public class Poupanca {
// DEPOSITO, TRANSFERENCIA E RESGATE
	
	private DecimalFormat df = new DecimalFormat("0.00");
	
	private Cliente cliente;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	private Integer id;
	
	@Column(name = "idCliente", nullable = false)
	private Integer idCliente;

	@Column(name = "numeroConta", unique = true, nullable = false)
	private int numeroConta;
	
	@Column(name = "valorEmConta", nullable = false)
	private double valorEmConta;
	
	public void consulta() {
		System.out.println("O saldo atual � de: R$" + df.format(this.valorEmConta));
	}
	
	public void deposito(double valor) {
		this.valorEmConta += valor;
		System.out.println("Foram depositados R$" + df.format(this.valorEmConta));
		this.consulta();
	}
	
	public void resgate(double valor) {
		//this.valorEmConta -= valor;
		// um m�todo que 
	}
	
	public void transferencia(int numeroOutraConta) {
		// transferencia
	}

}