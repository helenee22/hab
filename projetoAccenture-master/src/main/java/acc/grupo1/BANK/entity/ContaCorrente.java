package main.java.acc.grupo1.BANK.entity;

import java.text.DecimalFormat;

@Entity
@Table(name = "contaCorrente")
@Getter
@Setter
@NoArgsConstructor
public class ContaCorrente {
	
	private DecimalFormat df = new DecimalFormat("0.00");

	private Cliente cliente;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "valorEmConta", unique = true, nullable = false)
	private double valorEmConta;

	@Column(name = "numeroDaConta", nullable = false)
	private int numeroDaConta;
	
	@Column(name = "idCliente", nullable = false)
	private int idCliente;
	
	@Column(name = "nomeCliente", nullable = false)
	private String nomeCliente;
	
	@Column(name = "cpfCliente", nullable = false)
	private String cpfCliente;

	@Override
	public String toString() {
		return "Conta Corrente{" + "id=" + id + ", nomeCliente='" + cliente.getNome() + '\''
				+ ", valorEmConta='" + valorEmConta + '\'' + ", numeroDaConta='" + numeroDaConta + '\'' + '}';
	}
	
	public void consulta() {
		System.out.println("O saldo atual � de: R$" + df.format(this.valorEmConta));
	}
	
	public void deposito(double valor) {
		this.valorEmConta += valor;
		System.out.println("Foram depositados R$" + df.format(this.valorEmConta));
		this.consulta();
	}
	
	public void saque(double valor) {
		//this.valorEmConta -= valor;
		// um m�todo que 
	}
	
	public void transferencia(int numeroOutraConta) {
		// transferencia
	}

	public void recalcularSaldo() {
		// fazer
	}
	
	public void recebimentoSalario(double salario) {
		this.valorEmConta += valor;
		// nova transi�ao/extrato
	}
	
}