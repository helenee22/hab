package main.java.acc.grupo1.BANK.entity;

import java.text.DecimalFormat;
import java.util.Date;

@Entity
@Table(name = "cliente")
@Getter
@Setter
@NoArgsConstructor
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "cpf", unique = true, nullable = false)
	private String cpf;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "numeroDaConta", nullable = false)
	private int numeroDaConta;

	@Column(name = "data", nullable = false)
	private Date dataOperacao;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "password", nullable = false)
	private String password;

	public Cliente(String nome, String email, String password) {
		this.nome = nome;
		this.email = email;
		this.password = password;
	}

	@Override
	public String toString() {
		return "Cliente{" + "id=" + id + ", firstName='" + nome + '\'' + ", email='"
				+ email + '\'' + ", password='" + "*********" + '\'' + '}';
	}
}
