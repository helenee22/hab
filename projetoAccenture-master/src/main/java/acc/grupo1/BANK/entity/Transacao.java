package main.java.acc.grupo1.BANK.entity;

@Entity
@Table(name = "TRANSACAO")
@Getter
@Setter
@NoArgsConstructor
public class Transacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	private Integer id;
	
	@Column(name = "idClienteOrigem", nullable = false)
	private Integer idClienteOrigem;
	
	@Column(name = "tipoTransicao", nullable = false)
	private enumTransacao tipoTransacao;
	
	@Column(name = "numeroContaOrigem", nullable = false)
	private int numeroContaOrigem;
	
	@Column(name = "numeroContaOrigem", nullable = false)
	private int numeroContaDestino;
	
	@Column(name = "data", nullable = false)
	private Date data;
	
	@Column(name = "valorTransacao", nullable = false)
	private double valorTransacao;
	
	@Column(name = "saldoAposTransacao", nullable = false)
	private double saldoAposTransacao;
//	ID, Data e Hora, enumTransacao(compra, deposito, saque, transferenciaRecebida, tranferenciaRealizada), valorDaTransacao, saldoAposTransacao
}
